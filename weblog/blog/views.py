import json
from django.shortcuts import get_object_or_404, render, redirect
from django.http import HttpResponse
from django.http import JsonResponse
from django.views import generic
from .models import Post, Tag, Comment, Parent
from django.contrib.auth.forms import UserCreationForm
from django.urls import reverse_lazy
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User
from django.views.decorators.csrf import csrf_exempt, ensure_csrf_cookie
from django.db.models import Q


# class SignUp(generic.CreateView):
#     form_class = UserCreationForm
#     success_url = reverse_lazy('blog')
#     template_name = 'signup.html'


@csrf_exempt
def index(request):
    # post_list = Post.objects.filter(is_approve=True)
    post = Post.objects.get(id__exact=1)
    tag_list = Tag.objects.get(id__exact=3)
    comment_list = Comment.objects.get(id__exact=1)
    # context = {'post_list': post_list}
    # return render(request, 'blog/index.html', context) 
    return HttpResponse(post.title + ': ' + post.content + ' ** ' + str(tag_list) + ' ##### ' + str(comment_list))
    # return JsonResponse({'message': "ok"})

@csrf_exempt 
def signup(request):
    username = request.POST.get('username', False)
    password = request.POST.get('password', False)
    confirm_password = request.POST.get('confirm_password', False)
    if password != confirm_password:
        return JsonResponse({"success":False,"reason":"passwords don't match"})

    if username and password:
        user = User(username=username,password=password)  
        try:
            user.save()
        except:
            return JsonResponse({"success":False,"reason":"user already exists"})
        
        if user is not None:
            return redirect('/blog/')
        else:
            return JsonResponse({'message': "error"})
    else:
        return JsonResponse({"success":False,"reason":"username or password is invalid"})

@csrf_exempt
def createpost(request):
        title = request.POST.get('title', False)
        content = request.POST.get('content', False)
        if title and content:
            new_post = Post(title=title, content=content)
            new_post.save()
            new_post.tag.set(request.POST.get('tag'))
            return JsonResponse({'message': "ok"})
        else: 
            return JsonResponse({"success":False,"reason":"Post doesn't exist"})

@csrf_exempt
def createcomment(request, post_id):
    post = get_object_or_404(Post, pk=post_id)
    text = request.POST.get('text', False)
    if text:
        new_comment = Comment(text=text)
        new_comment.post = post
        new_comment.save()
        return JsonResponse({'message': "ok"})
    else: 
        return JsonResponse({"success":False,"reason":"Comment doesn't exist"})
