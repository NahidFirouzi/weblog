
from django.urls import path, include
from . import views

app_name = 'blog'
urlpatterns = [
    path('', views.index, name='index'),
    path('signup/', views.signup, name='signup'),
    path('createpost/', views.createpost, name='createpost'),
    path('<int:post_id>/createcomment/', views.createcomment, name='createcomment'),
]