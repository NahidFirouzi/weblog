from django.db import models
from django.contrib.auth.models import User
from django.db.models import F
import re

STATUS = (
    (0,"reject"),
    (1,"step1"),
    (2,"step2"),
    (3,"step3"),
    (4,"step4"),
    (5,"approved")
)


class Tag(models.Model):
    name = models.CharField(max_length=50)
    def __str__(self):
        return self.name

class Post(models.Model):
    title = models.CharField(max_length=200)
    content = models.TextField()
    tag = models.ManyToManyField(Tag, blank=True)
    status = models.IntegerField(choices=STATUS, default=1)
    is_approve = models.BooleanField(default=False)

    # def get_tag_list(self):
    #     return re.split(" ", self.tags)
    def __str__(self):
        return self.title

class Parent(models.Model):
    reply = models.CharField(max_length=200)
    def __str__(self):
        return self.reply

class Comment(models.Model):
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    text = models.TextField()
    like = models.IntegerField(default=0)
    dislike = models.IntegerField(default=0)
    parent = models.ForeignKey(Parent, on_delete=models.CASCADE, null=True, blank=True, related_name='replies')
    

    class Meta:
    	ordering = [-(F('like') - F('dislike'))]

    def __str__(self):
        return self.text