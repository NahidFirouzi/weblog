from django.contrib import admin
from .models import Post, Comment, Tag, Parent
import io
from django.contrib.admin import SimpleListFilter
from django.db.models import F
from django.db.models import Q

class CommentInline(admin.TabularInline):
    model = Comment
    extra = 1

class StatusFilter(SimpleListFilter):
    title = 'status'
    parameter_name = 'rejected'

    def lookups(self, request, model_admin):
        return [('rejected', 'Rejected posts'),('not_rejected', 'Not rejected'),]
    
    def queryset(self, request, queryset):

        if self.value() == 'rejected':
            return queryset.filter(status=0)

        if self.value():
            return queryset.exclude(status=0)


class PostAdmin(admin.ModelAdmin):
    fieldsets = [
        ('Post information', {'fields': ['title' , 'content','tag', 'status', 'is_approve']}),
    ]
    inlines = [(CommentInline)]
    list_display = ('title','status','id')
    list_filter = [('is_approve'),(StatusFilter)]
    actions = [("approve"), ("reject"),("Return")]

    def approve(self,requst,queryset):
        if queryset.filter(Q(is_approve=False) & Q(status=5)):
            return queryset.update(is_approve=True)

        elif queryset.filter(Q(is_approve=True) & Q(status=5)):
            queryset.none()

        elif queryset.filter(Q(is_approve=False) & Q(status=0)):
            queryset.none()

        else:
            queryset.update(status=F('status')+1)
            if queryset.filter(Q(is_approve=False) & Q(status=5)):
                return queryset.update(is_approve=True)


    approve.short_description = "Approve the post"

    def Return(self,requst,queryset):
        if queryset.filter(is_approve=False):
            if queryset.exclude(status=0):
                queryset.update(status=F('status')-1)
        else:
            queryset.update(status=F('status')-1)
            queryset.update(is_approve=False)

    Return.short_description = "Return the post"        
    
    def reject(self,requst,queryset):
        queryset.update(is_approve=False)
        queryset.update(status=0)

    reject.short_description = "Reject the post"

admin.site.register(Post,PostAdmin)
admin.site.register(Tag)
admin.site.register(Parent)
admin.site.register(Comment)